/**
 * 
 */
package uo.harish.prakash.sycamore.plugins.algorithms;

import java.util.Vector;
import it.diunipi.volpi.sycamore.engine.Observation;
import it.diunipi.volpi.sycamore.engine.Point2D;
import it.diunipi.volpi.sycamore.engine.SycamoreEngine.TYPE;
import it.diunipi.volpi.sycamore.engine.SycamoreObservedRobot;
import it.diunipi.volpi.sycamore.gui.SycamorePanel;
import it.diunipi.volpi.sycamore.plugins.algorithms.AlgorithmImpl;
import net.xeoh.plugins.base.annotations.PluginImplementation;

/**
 * @author harry
 *
 */
@PluginImplementation
public class AlgorithmAverageCoordinate extends AlgorithmImpl<Point2D> {

	private String	_author				= "Harish Prakash";
	private TYPE	_type				= TYPE.TYPE_2D;
	private String	_shortDescription	= "Average Coordinate Move";
	private String	_pluginName			= "AverageCoordinate";
	private String	_description		= "Robots move to the average coordinate";

	// Generic Inherited properties
	@Override
	public TYPE getType() {

		return _type;
	}

	@Override
	public String getAuthor() {

		return _author;
	}

	@Override
	public String getPluginShortDescription() {

		return _shortDescription;
	}

	@Override
	public String getPluginLongDescription() {

		return _description;
	}

	@Override
	public SycamorePanel getPanel_settings() {

		return null;
	}

	@Override
	public String getPluginName() {

		return _pluginName;
	}
	// END OF Generic Inherited Properties

	// Generic Inherited Properties of an algorithm
	@Override
	public void init(SycamoreObservedRobot<Point2D> robot) {}

	@Override
	public String getReferences() {

		return null;
	}
	// END of Generic Inherited Properties of an algorithm

	@Override
	public Point2D compute(Vector<Observation<Point2D>> observations, SycamoreObservedRobot<Point2D> caller) {

		Point2D callerPosition = caller.getLocalPosition();
		boolean terminateVerified = true;

		double x = callerPosition.x;
		double y = callerPosition.y;

		for (Observation<Point2D> observation : observations) {
			Point2D position = observation.getRobotPosition();
			terminateVerified = terminateVerified && !callerPosition.differsModuloEpsilon(position);

			x += position.x;
			y += position.y;
		}

		x = x / (observations.size() + 1);
		y = y / (observations.size() + 1);

		setFinished(terminateVerified);
		return new Point2D((float) x, (float) y);
	}

}
